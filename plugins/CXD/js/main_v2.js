define([
           'dojo/_base/declare',
	   'dojo/_base/lang',
	   'dojo/io-query',
           'JBrowse/Plugin',
           'dijit/MenuItem',
       ],
       function(
           declare,
	   lang,
	   ioQuery,
           JBrowsePlugin,
	   dijitMenuItem
       ) {
 
return declare( JBrowsePlugin,
{
    constructor: function( args ) {
        var browser = this.browser;
 
        /* do anything you need to initialize your plugin here */
	console.log("JHH plugin work!");
	
        this.browser.afterMilestone('initView', function() {
        	this.browser.addGlobalMenuItem( 'cxd', new dijitMenuItem(
                                           {
                                               label: 'printStatus',
                                               iconClass: 'dijitIconBookmark',
                                               onClick: lang.hitch(this, 'printStatus')
                                           }));
        	this.browser.addGlobalMenuItem( 'cxd', new dijitMenuItem(
                                           {
                                               label: 'rangeSelect',
                                               iconClass: 'dijitIconBookmark',
                                               onClick: lang.hitch(this, 'rangeSelect')
                                           }));
        	this.browser.addGlobalMenuItem( 'cxd', new dijitMenuItem(
                                           {
                                               label: 'cxd_getSeq',
                                               iconClass: 'dijitIconBookmark',
                                               onClick: lang.hitch(this, 'cxd_getSeq')
                                           }));
        	this.browser.addGlobalMenuItem( 'cxd', new dijitMenuItem(
                                           {
                                               label: 'cxd_getFeature',
                                               iconClass: 'dijitIconBookmark',
                                               onClick: lang.hitch(this, 'cxd_getFeature')
                                           }));
        	this.browser.addGlobalMenuItem( 'cxd', new dijitMenuItem(
                                           {
                                               label: 'Capture',
                                               iconClass: 'dijitIconBookmark',
                                               onClick: lang.hitch(this, 'cxd_capture')
                                           }));
        	this.browser.addGlobalMenuItem( 'cxd', new dijitMenuItem(
                                           {
                                               label: 'Navi',
                                               iconClass: 'dijitIconBookmark',
                                               onClick: lang.hitch(this, 'cxd_navi')
                                           }));


        	this.browser.renderGlobalMenu( 'cxd', {text: 'JHH'}, browser.menuBar );
		console.log(this.browser);
        }, this );
    },

    printStatus: function() {
	var queryParams = ioQuery.queryToObject( window.location.search.slice(1) );
	console.log(queryParams);
    },

    rangeSelect: function() {
	console.log("select!!!");
    },

    cxd_getSeq: function() {
	console.log("getSeq!!!");
        //this.browser.getStore('refseqs', dojo.hitch(this,function( refSeqStore ) {
        this.browser.getStore('refseqs', function( refSeqStore ) {
		console.log("popo~", refSeqStore);
			
		refSeqStore.getReferenceSequence(
                         { ref: 'ctgA', start: 0, end: 100},
                         // feature callback
                         dojo.hitch( this, function( seq ) {
                             console.log("kid", seq);
                       }),
                       // end callback
                       function() {},
                       // error callback
                       dojo.hitch( this, function() {
                       })
                     );

	//}));
	});
    },

    cxd_getFeature: function() {
	console.log("getFeature!!!");
	console.log("tracks shown now",this.browser.view.tracks);
	var my_tracks = this.browser.view.tracks;

	var query = {
		ref: "ctgA",
		start: 9800,
		end: 25000
	};
	my_tracks[1].store.getFeatures(query, function(arg1){
	}, function() {
	});
	
	query = {
		ref: "ctgA",
		start: 5000,
		end: 6000
	};
	my_tracks[2].store.getFeatures(query, function(arg1){
	}, function() {
	});
    },

    cxd_capture: function() {
			var queryParams = ioQuery.queryToObject( window.location.search.slice(1) );
			console.log(queryParams);
			var target = queryParams.highlight;
			target = target.split(/[:(\.\.)]/);
			console.log(target);
			var query = {
				ref : target[0],
				start : target[1],
				end : target[3]
			};
			
			if (query.ref == "") {
				console.log("No query");
				return;
			}

			var my_tracks = this.browser.view.tracks;

			my_tracks[0].store.getReferenceSequence(query, function(seq) {
				set_text("B9", seq);
			});

			for (var i = 1; i < my_tracks.length; i++) {
				var num = 1;
				if (my_tracks[i].config.type == "JBrowse/View/Track/HTMLFeatures") {
						my_tracks[i].store.getFeatures(query, function(f) {
							/*
							for (var j = 0; j < f.length - 1; j++) {
								var col = column_trans(num);
								var row = (j+1);
								var pos = col + row;
								set_text(pos, f[j]);
							}
							*/
							num = num + 1;
						}, function() { //success
						});
				} else if (my_tracks[i].config.type == "JBrowse/View/Track/Wiggle/Density") {
						var row = 1;
						my_tracks[i].store.getFeatures(query, function(f) {
							set_text("A" + row, parseInt(f.get("score")) / 10);
							row = row + 1;
						});
				} else if (my_tracks[i].config.type == "JBrowse/View/Track/Alignments2") {
						var row = 1;
						console.log(f);
						my_tracks[i].store.getFeatures(query, function(f) {
							set_text("K" + row, f.seq());
							row = row + 1;
						});
						console.log("number", row);
				}
			}
    },

    cxd_navi: function() {
		console.log("cxd navi!!!");
		console.log(this.browser.getHightlight());	
    },
});
 
});
